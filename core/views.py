from django.shortcuts import render, HttpResponse, redirect
from core.forms import (
    RegistrationForm,
    EditProfileForm
)
from django.contrib.auth.forms import UserChangeForm, PasswordChangeForm
from django.contrib.auth.models import User
from django.contrib.auth import update_session_auth_hash


def home(request):
    numbers = {1,2,3,4,5}
    name= 'Filip Karwwoski'
    
    args = {'myName': name, 'numbers': numbers}
    
    return render(request,'core/home.html', args)

def register(request):
    
    if(request.method =='POST'):
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
    
    else:
        form = RegistrationForm()
        
        args = {'form': form}
        return render(request, 'core/reg_form.html', args)
    
def view_profile(request):
    args = {'user': request.user}
    return render(request, 'core/profile.html', args)

def edit_profile(request):
    if request.method == 'POST':
        form = EditProfileForm(request.POST, instance=request.user)
        
        if form.is_valid():
            form.save()
            return redirect('/core/profile')
    else:
        form = EditProfileForm(instance=request.user)
        args = {'form': form}
        return render(request, 'core/edit_profile.html', args)

def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)
        
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect('/core/profile')
        else:
            return redirect('/core/change-password/')
    else:
        form = PasswordChangeForm(user=request.user)
        args = {'form': form}
        return render(request, 'core/change_password.html', args)